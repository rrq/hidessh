#!/bin/bash
#
# This script sets up an ipset and iptables rules so that the SSH port
# only opens via a "secret handshake".

[ "$UID" = 0 ] || exec sudo -- $0 $*

# $1 = -A for adding rules, or -D for deleting rules
function firewall() {
    local GATE6=${GATE}6

    # Create $GATE set with a default timeout of $TM1 seconds
    if [ "$1" = "-A" ] ; then
	ipset create $GATE hash:ip timeout $TM1 || echo ignored
	ipset create $GATE6 hash:ip family inet6 timeout $TM1 || echo ignored
    fi

    ## Firewall rules ##
    #
    # 1) Inbound TCP traffic to port $SSHPORT by $GATE IP updates to
    # keep it open another $TM2 seconds
    #
    # 2) Then any traffic through by $GATE IP is accepted
    #
    # 3) Otherwise any traffic to port $SSHPORT is dropped.
    #
    # 3) For a UDP packet with '$HTEXT' to port $HPORT, add the source
    # IP to the $GATE set with $TM1 seconds timeout
    # Note: ipv4 payload starts at 28 and ipv6 payload starts at 48
    #
    # Outbound TCP traffic from port $SSHPORT updates to keep open for
    # the remote IP in $GATE another $TM2 seconds.
    
    cat <<EOF | iptables-restore -n
*filter
$1 INPUT -p tcp -m tcp --dport $SSHPORT -m set --match-set $GATE src -j SET --add-set $GATE src --exist --timeout $TM2
$1 INPUT -m set --match-set $GATE src -j ACCEPT
$1 INPUT -p tcp -m tcp --dport $SSHPORT -j DROP
$1 INPUT -p udp -m udp --dport $HPORT -m string --string "$HTEXT" --algo bm --from 28 --to 65535 -j SET --add-set $GATE src --timeout $TM1
$1 OUTPUT -p tcp -m tcp --sport $SSHPORT -j SET --add-set $GATE dst --exist --timeout $TM2
COMMIT
EOF
    cat <<EOF | ip6tables-restore -n
*filter
$1 INPUT -p tcp -m tcp --dport $SSHPORT -m set --match-set $GATE6 src -j SET --add-set $GATE6 src --exist --timeout $TM2
$1 INPUT -m set --match-set $GATE6 src -j ACCEPT
$1 INPUT -p tcp -m tcp --dport $SSHPORT -j DROP
$1 INPUT -p udp -m udp --dport $HPORT -m string --string "$HTEXT" --algo bm --from 48 --to 65535 -j SET --add-set $GATE6 src --timeout $TM1
$1 OUTPUT -p tcp -m tcp --sport $SSHPORT -j SET --add-set $GATE6 dst --exist --timeout $TM2
COMMIT
EOF
    # Destroy the $GATE set
    if [ "$1" = "-D" ] ; then
	echo "Destroying $GATE and $GATE6" >&2
	ipset destroy $GATE
	ipset destroy $GATE6
    fi
    
}

configure() {
    read -e -p "ssh port: " -i $SSHPORT SSHPORT
    read -e -p "ipset set: " -i $GATE GATE
    read -e -p "login timeout (seconds): " -i $TM1 TM1
    read -e -p "idle time (seconds): " -i $TM2 TM2
    read -e -p "handshake port: " -i $HPORT HPORT
    read -e -p "handshake secret: " -i $HTEXT HTEXT
    cat <<EOF > $HOME/.hidesshrc
# Configuration for hidessh.sh
# SSHPORT = The ssh port
# GATE = the "gate" set
# TM1 = its short timeout, in seconds
# TM2 = its long timeout, in seconds
# HPORT = the handshake port
# HTEXT = the "secret" cookie for access
SSHPORT=$SSHPORT
GATE=$GATE
TM1=$TM1
TM2=$TM2
HPORT=$HPORT
HTEXT='$HTEXT'
EOF
}

##============================================================
## Main
##

# Setup defaults
SSHPORT=22
GATE=GATE
TM1=5
TM2=600
HPORT=20
HTEXT='hello'

# override with configuration, if any
[ -r $HOME/.hidesshrc ] && . $HOME/.hidesshrc

GATE6=${GATE}6

case $1 in
    setup)
	configure
	;;
    start)
	firewall -A
	;;
    stop)
	firewall -D
	;;
    *)
	echo "use 'setup', 'start' or 'stop'" >&2
	exit 1
	;;
esac
